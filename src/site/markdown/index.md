# Samply MDR Catalog XSD

This project contains an XSD file for catalogs, that can be imported into the MDR.


## Build

Use maven to build the jar:

```
mvn clean package
```

Use maven to generate the classes using the XSD files:

```
mvn jaxb2:generate
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.samply</groupId>
    <artifactId>mdr-catalog-xsd</artifactId>
    <version>VERSION</version>
</dependency>
```
