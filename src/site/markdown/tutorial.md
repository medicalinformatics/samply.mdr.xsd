# MDR Catalog XSD - Usage

The XSD file describes the XML-format that the MDR understand. You can create
catalogs using this XML format and upload them on the MDR.

Here is a small example catalog:

```
<?xml version="1.0" encoding="UTF-8"?>
<catalog>
  <definitions>
    <definition language="en">
      <designation>Example catalog</designation>
      <text>A simple example catalog</text>
    </definition>
  </definitions>

  <code code="m" isValid="true">
    <definitions>
      <definition language="en">
        <designation>male</designation>
        <text>Specifies the gender as male.</text>
      </definition>
    </definitions>
  </code>

  <code code="f" isValid="true">
    <definitions>
      <definition language="en">
        <designation>female</designation>
        <text>Specifies the gender as female.</text>
      </definition>
    </definitions>
  </code>

  <code code="u" isValid="true">
    <definitions>
      <definition language="en">
        <designation>unknown</designation>
        <text>Specifies the gender as unknown.</text>
      </definition>
    </definitions>
  </code>
</catalog>
```
